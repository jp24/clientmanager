import { HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse } from "@angular/common/http";
import { MessageService } from "./message.service";
import { catchError } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";

@Injectable()
export class ErrorHandlingInterceptor implements HttpInterceptor{
    constructor(private messageService: MessageService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        console.log(req);
        return next.handle(req).pipe(catchError(error => {
            if(error instanceof HttpErrorResponse)
                this.messageService.error(`Błąd połączenia ${error.message}`);
            return Observable.throw(error);
        }))
    }
    
}