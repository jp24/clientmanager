import { NgModule } from '@angular/core';
import { CounterService } from './counter.service';
import { ErrorHandlingInterceptor } from './error-handling.interceptor';
import { MessageService } from './message.service';
import { Config, CONFIG } from '../model';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';

const config: Config = {
  customerLimit: 10,
  apiUrl: "http://localhost:13378"//zmiana z 13378 na 8080
};


@NgModule({
  imports: [
    CommonModule,//może być SharedModule
    RouterModule
  ],
  providers: [
    CounterService,
    ErrorHandlingInterceptor,
    MessageService,
    {provide: CONFIG, useValue: config},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorHandlingInterceptor, multi: true},
    AuthGuard,
    AuthService
  ],
  declarations: [NavbarComponent, NotFoundComponent],
  exports: [NavbarComponent]
})
export class CoreModule { }
