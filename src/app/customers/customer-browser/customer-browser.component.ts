import { Component, OnInit, ViewChild } from '@angular/core';
import { Customer, CustomerType } from '../model';
import { CustomerDetailsComponent } from '../customer-details/customer-details.component';
import { CustomerService } from '../customer.service';
import { CounterService } from 'src/app/core/counter.service';
import { MessageService } from 'src/app/core/message.service';

@Component({
  selector: 'cus-customer-browser',
  templateUrl: './customer-browser.component.html',
  styleUrls: ['./customer-browser.component.css']
})
export class CustomerBrowserComponent implements OnInit {
  
  customers: Customer[];
  customer: Customer;

  constructor(private customerService: CustomerService, private counterService: CounterService, private messagrService: MessageService) { }

  @ViewChild("details")
  detailsComponent: CustomerDetailsComponent;

  ngOnInit() {
    
    this.refresh();
    this.counterService.increase();
  }

  changeColor(){
    this.detailsComponent.changeHeaderColor();
  }

  onShift(direction: string){
    const idx = this.customers.indexOf(this.customer);
    if(idx > 0 && direction === "left")
      this.customer = this.customers[idx-1];
    else if(direction === "right" && idx < this.customers.length-1)
      this.customer = this.customers[idx+1];
  }

  deleteCustomer(){
    this.customerService.deleteCustomer(this.customer).subscribe(
      ()=>{
        this.messagrService.success("Udało się usunąć klienta.");
        this.refresh();
      },
      error =>{
        console.log(error);
        this.messagrService.error("Błąd w połączeniu z serwerem !");
      }
    );
  }

  private refresh(){
    this.customerService.getCustomers().subscribe(response => {
      this.customers = response;
      this.customer = null;
    });
    // lokalny plik - załadowanie
    // this.customerService.getJson().subscribe(response =>{
    //   this.customers = response;
    // });
  }
}
