import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import 'rxjs/add/operator/map';
import { map, catchError } from 'rxjs/operators';
import { CONFIG, Config } from '../model';
import { Customer } from './model';
import { CounterService } from '../core/counter.service';

@Injectable()
export class CustomerService {

  constructor(private counterService: CounterService, @Inject(CONFIG) private config: Config, private httpClient: HttpClient) { }

  getCustomers(){//zmiana z /customers na /rest-service/webapi/users/customers
    return this.httpClient.get<Customer[]>(`${this.config.apiUrl}/customers`)
    .pipe(map(customers => customers.slice(0, this.config.customerLimit)));
  }

  // getJson(){                //ładowanie z pliku lokalnego
  //   return this.httpClient.get<Customer[]>('assets/customers2.json');
  //   //.pipe(map(customers => customers.slice(0, this.config.customerLimit)));
  // }

  createCustomer(customer: Customer){
    return this.httpClient.post(`${this.config.apiUrl}/customers`, customer);
    //.pipe(catchError(this.handleError));//zmiana z /customers
  }

  deleteCustomer(customer: Customer){
    return this.httpClient.delete(`${this.config.apiUrl}/customers/${customer.id}`);
  }

  // public handleError(res: Response) {
  //   return Observable.throw(res);
  //   }
}
