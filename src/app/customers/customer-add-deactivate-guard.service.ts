import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CustomerAddComponent } from './customer-add/customer-add.component';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class CustomerAddDeactivateGuard implements CanDeactivate<CustomerAddComponent>{
  canDeactivate(component: CustomerAddComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextstate?: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{
      return !(component.name || component.age || component.type);
  }

  constructor() { }
}
