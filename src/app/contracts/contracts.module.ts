import { NgModule } from '@angular/core';
import { ContractListComponent } from './contract-list/contract-list.component';
import { ContractService } from './contract.service';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ContractDetailsComponent } from './contract-details/contract-details.component';
import { ContractResolver } from './contract-resolver.service';

const routes = [
  { path: ':id', component: ContractDetailsComponent, resolve: {contract: ContractResolver}},/*'contracts/:id'*/
  { path: '', component: ContractListComponent }/*'contracts'*/
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [ContractListComponent, ContractDetailsComponent],
  providers: [ContractService, ContractResolver],
  exports: [ContractListComponent]
})
export class ContractsModule { }
