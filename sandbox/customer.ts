import { LogClass, Log } from "./decorators";

//moduły
// klasa w osobnym pliku .ts

@LogClass("Info")
export class _Customer{
    name: string;
    age: number;

    constructor(name: string, age: number){
        this.name = name;
        this.age = age;
    }

    @Log()
    welcomeFromClass(){
        console.log(`Hello ${this.name}, age ${this.age}`);
    }

    isAdult(){
        if(this.age >= 18)
            return true;
        else
            return false;
    }
}

export let persons = [
    new _Customer("Jan", 32),
    new _Customer("Michał", 37),
    new _Customer("Jacek", 39)
];

export function welcomeFunc(){
    console.log("hello !");
}

