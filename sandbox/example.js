console.log("Hello !!!");
var x = 3;
var y = "napis";
console.log(x, y);
//tablice
var xs = [1, 2, 3];
console.log(xs.length);
var tab = [5.6, 7.8, 9.9].concat(xs);
console.log(tab);
//interpolacja stringow
var zm = "x wynosi: " + x;
console.log(zm);
console.log("pierwszy element tablicy xs: " + xs[0]);
//funkcje
function welcome(name) {
    console.log("Welcome " + name);
}
welcome("Pola");
function powitanie(name, age) {
    if (age === void 0) { age = 35; }
    return "Witaj " + name + " " + age + " !!!";
}
console.log(powitanie("Michał", 55));
function welcomeMany() {
    var names = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        names[_i] = arguments[_i];
    }
    return "welcome: " + names.join(", ");
}
console.log(welcomeMany("Pola", "Janek", "Michał", "Lucy", "Cindy"));
function welcomeWithReturn(name) {
    return "welcome " + name + " !";
}
console.log(["Lucy", "Cindy"].map(welcomeWithReturn));
//funkcja anonimowa
console.log(["Lucy", "Cindy"].map(function (name) {
    return "Welcome " + name + " !!!";
}));
//funkcja anonimowa (arrow function)
console.log(["Lucy", "Cindy"].map(function (name) {
    return "Welcome " + name + " !!!";
}));
function welcomeWithInterface(customer) {
    return "Welcome " + customer.name + " !!!";
}
console.log(welcomeWithInterface({
    name: "Janek",
    address: {
        street: "Wojska Polskiego",
        city: "Opole"
    }
}));
//instrukcje sterujące
var kTab = [6, 8, 10];
for (var _i = 0, kTab_1 = kTab; _i < kTab_1.length; _i++) {
    var n = kTab_1[_i];
    console.log(n);
}
//enum
var CustomerType;
(function (CustomerType) {
    CustomerType[CustomerType["Standard"] = 0] = "Standard";
    CustomerType[CustomerType["Premium"] = 1] = "Premium";
    CustomerType[CustomerType["VIP"] = 2] = "VIP";
})(CustomerType || (CustomerType = {}));
;
function welcomeWithEnum(name, ct) {
    if (ct == CustomerType.VIP)
        console.log("Welcome dear " + name + " !!!");
}
welcomeWithEnum("Michał", 2);
