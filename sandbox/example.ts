//import {_Customer, persons, welcomeFunc} from './customer';
import {Observable} from 'rxjs';
import * as _Customer from './customer';


console.log("Hello !!!");

let x:number = 3;

let y:string = "napis";

console.log(x, y);

//tablice

let xs = [1,2,3];

console.log(xs.length);

let tab:Array<number> = [5.6, 7.8, 9.9, ...xs];

console.log(tab);

//interpolacja stringow

let zm = `x wynosi: ${x}`;

console.log(zm);

console.log(`pierwszy element tablicy xs: ${xs[0]}`);

//funkcje
function welcome(name:string){
    console.log(`Welcome ${name}`);
}

welcome("Pola");

function powitanie(name:string, age:number = 35):string{
    return `Witaj ${name} ${age} !!!`;
}

console.log(powitanie("Michał", 55));

function welcomeMany(...names: string[]):string{
    return `welcome: ${names.join(", ")}`;
}

console.log(welcomeMany("Pola", "Janek", "Michał", "Lucy", "Cindy"));

function welcomeWithReturn(name:string):string{
    return `welcome ${name} !`;
}

console.log(["Lucy", "Cindy"].map(welcomeWithReturn));

//funkcja anonimowa
console.log(["Lucy", "Cindy"].map(function(name: string): string{
    return `Welcome ${name} !!!`;
}));

//funkcja anonimowa (arrow function)
console.log(["Lucy", "Cindy"].map(name =>
    `Welcome ${name} !!!`
));

//interfejsy

interface Customer{
    name: string,
    age?: number
    address: Address
}

function welcomeWithInterface(customer: Customer): string{
    return `Welcome ${customer.name} !!!`;
}

console.log(welcomeWithInterface({
    name: "Janek",
    address: {
        street: "Wojska Polskiego",
        city: "Opole"
    }
}));

interface Address{
    street: string,
    city: string
}

//instrukcje sterujące

let kTab = [6,8,10];

for(let n of kTab)
    console.log(n);

//enum

enum CustomerType{
    Standard,
    Premium,
    VIP
};

function welcomeWithEnum(name: string, ct: CustomerType){
    if(ct == CustomerType.VIP)
        console.log(`Welcome dear ${name} !!!`);
}

welcomeWithEnum("Michał", 2);

//klasy
// class _Customer{
//     name: string;
//     age: number;

//     constructor(name: string, age: number){
//         this.name = name;
//         this.age = age;
//     }

//     welcomeFromClass(){
//         console.log(`Hello ${this.name}, age ${this.age}`);
//     }

//     isAdult(){
//         if(this.age >= 18)
//             return true;
//         else
//             return false;
//     }
// }

let _customer = new _Customer._Customer("Jacek", 40);

_customer.welcomeFromClass();
console.log(_customer.isAdult());

console.log(new _Customer._Customer("Jan", 25).isAdult());

class Vehicle{
    type: string;
    engine: string;
    drive: string;

    constructor(type: string, engine: string, drive: string){
        this.type = type;
        this.engine = engine;
        this.drive = drive;
    }

    show(){
        console.log(`Vehicle type: ${this.type}\nengine: 
        ${this.engine}\ndrive: ${this.drive}`);
    }
} 

class Car extends Vehicle{
    maker: string;
    power: number;
    litres: number;

    constructor(type: string, engine: string, drive: string, 
        maker: string, power: number, litres: number){
        super(type, engine, drive);
        this.maker = maker;
        this.power = power;
        this.litres = litres;
    }

    show(){
        console.log(`Car maker: ${this.maker}\npower: ${this.power}\nlitres ${this.litres}`);
    }
}

let car = new Car("passenger", "gasoline", "FWD", "Ford", 90, 1.8);
car.show();

let car2: Vehicle = new Car("passenger", "gasoline", "FWD", "Nissan", 130, 2.0);
car2.show();

//modyfikatory dostępu i interfejs
interface canTalk{
    talk(): string;
}

class Person implements canTalk{
    protected name: string;
    age: number;

    constructor(name: string, age: number){
        this.name = name;
        this.age = age;
    }

    welcome(){
        return `Hello ${this.name}, ${this.age}`;
    }

    talk(): string{
        return "talk";
    }
}

class Customer2 extends Person{
    advisor: string;

    constructor(name: string, age: number, advisor: string){
        super(name, age);
        this.advisor = advisor;
    }

    welcome(){
        return `Good morning ${this.name}`;
    }
}

let customer2 = new Customer2("Pola", 40, "Michał");

console.log(customer2.welcome());
console.log(customer2.talk());
//modyfikatory dostępu skrócenie kodu

// class Person{

//     constructor(protected name: string, public age: number){}

//     welcome(){
//         return `Hello ${this.name}, ${this.age}`;
//     }
// }

// class Customer2 extends Person{

//     constructor(name: string, age: number, public advisor: string){
//         super(name, age);
//     }

//     welcome(){
//         return `Good morning ${this.name}`;
//     }
// }

//wykorzystanie metody z biblioteki rxjs
Observable.toString();

console.log(_Customer.persons);

_Customer.welcomeFunc();